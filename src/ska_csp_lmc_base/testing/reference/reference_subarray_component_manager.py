"""This module models component management for CSP subelement observation devices."""

from __future__ import annotations

import json
import logging
import threading
from typing import Any, Callable, List, Optional

from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_tango_base.base import (
    CommunicationStatusCallbackType,
    check_communicating,
    check_on,
)
from ska_tango_base.testing.reference.reference_base_component_manager import (
    FakeBaseComponent,
    GenericBaseComponentManager,
)

from ...subarray import CspSubarrayComponentManager


class FakeCspSubarrayComponent(FakeBaseComponent):
    """
    A fake component for the component manager to work with.

    NOTE: There is usually no need to implement a component object.
    The "component" is an element of the external system under
    control, such as a piece of hardware or an external service. In the
    case of a subarray device, the "component" is likely a collection of
    Tango devices responsible for monitoring and controlling the
    various resources assigned to the subarray. The component manager
    should be written so that it interacts with those Tango devices. But
    here, we fake up a "component" object to interact with instead.

    It supports the configure`, `scan`, `end_scan`, `end`, `abort`,
    `obsreset` and `restart` methods. For testing purposes, it can also
    be told to simulate a spontaneous state change via
    `simulate_power_state` and `simulate_fault` methods.

    When one of these command methods is invoked, the component
    simulates communications latency by sleeping for a short time. It
    GenericBaseComponentManager
    then returns, but simulates any asynchronous work it needs to do by
    delaying updating task and component state for a short time.
    """

    resources_fqdn = [
        "reference/resource/01",
        "reference/resource/02",
        "reference/resource/03",
        "reference/resource/04",
    ]

    def __init__(  # pylint: disable=too-many-arguments
        self: FakeCspSubarrayComponent,
        time_to_return: float = 0.05,
        time_to_complete: float = 0.4,
        power: PowerState = PowerState.OFF,
        fault: Optional[bool] = None,
        resourced: bool = False,
        configured: bool = False,
        scanning: bool = False,
        obsfault: bool = False,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param time_to_return: the amount of time to delay before
            returning from a command method. This simulates latency in
            communication.
        :param time_to_complete: the amount of time to delay before the
            component calls a task callback to let it know that the task
            has been completed
        :param power: initial power state of this component
        :param fault: initial fault state of this component
        :param resourced: initial resourced state of this component
        :param configured: initial configured state of this component
        :param scanning: initial scanning state of this component
        :param obsfault: initial obsfault state of this component
        :param kwargs: additional keyword arguments
        """
        self._config_id = ""
        self._scan_id = 0
        self._last_scan_configuration = ""
        self._assigned_resources: List[str] = []

        super().__init__(
            time_to_return=time_to_return,
            time_to_complete=time_to_complete,
            power=power,
            fault=fault,
            resourced=resourced,
            configured=configured,
            scanning=scanning,
            obsfault=obsfault,
            **kwargs,
        )

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_on
    def config_id(self: FakeCspSubarrayComponent) -> str:
        """
        Return the unique id of this configuration.

        :return: a unique id
        """
        return self._config_id

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_on
    def scan_id(self: FakeCspSubarrayComponent) -> int:
        """
        Return the unique id of this scan.

        :return: a unique id
        """
        return self._scan_id

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_on
    def last_scan_configuration(self: FakeCspSubarrayComponent) -> str:
        """
        Return the last programmed configuration.

        :return: a JSON formatted string
        """
        return self._last_scan_configuration

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_on
    def assigned_resources(self: FakeCspSubarrayComponent) -> List:
        """
        Return the list of assigned resources IDs.

        :return: a list of str
        """
        return self._assigned_resources

    @check_on
    def assign(
        self: FakeCspSubarrayComponent,
        resources: set[int],
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Assign resources.

        :param resources: the resources to be assigned.
        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Resource assignment failed; component is in fault.",
            )
        else:
            result = (ResultCode.OK, "Resource assignment completed OK")
            for resource_id in resources:
                if self.resources_fqdn[resource_id - 1] not in self._assigned_resources:
                    self._assigned_resources.append(
                        self.resources_fqdn[resource_id - 1]
                    )

        self._simulate_task_execution(
            task_callback,
            task_abort_event,
            result,
            resourced=True,
        )

    @check_on
    def release(
        self: FakeCspSubarrayComponent,
        resources: dict,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Release resources.

        :param resources: resources to be released
        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Resource release failed; component is in fault.",
            )
        else:
            if self._assigned_resources:
                for resource_id in resources:
                    if self.resources_fqdn[resource_id - 1] in self._assigned_resources:
                        self._assigned_resources.remove(
                            self.resources_fqdn[resource_id - 1]
                        )
            result = (ResultCode.OK, "Resource release completed OK")

        self._simulate_task_execution(
            task_callback,
            task_abort_event,
            result,
            resourced=True,
        )

    @check_on
    def release_all(
        self: FakeCspSubarrayComponent,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Release all resources.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Resource release failed; component is in fault.",
            )
        else:
            result = (ResultCode.OK, "Resource release completed OK")

        self._simulate_task_execution(
            task_callback, task_abort_event, result, resourced=False
        )

    @check_on
    def configure(
        self: FakeCspSubarrayComponent,
        configuration: Any,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Configure the component.

        :param configuration: the configuration to be configured
        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        self._config_id = configuration.get("config_id")
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Configure failed; component is in fault.",
            )
        else:
            self._last_scan_configuration = json.dumps(configuration)
            result = (ResultCode.OK, "Configure completed OK")

        self._simulate_task_execution(
            task_callback, task_abort_event, result, configured=True
        )

    @check_on
    def deconfigure(
        self: FakeCspSubarrayComponent,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Deconfigure this component.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Deconfigure failed; component is in fault.",
            )
        else:
            self._config_id = ""
            self._last_scan_configuration = ""
            result = (ResultCode.OK, "Deconfigure completed OK")
        self._simulate_task_execution(
            task_callback, task_abort_event, result, configured=False
        )

    @check_on
    def scan(
        self: FakeCspSubarrayComponent,
        scan_id: int,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Start scanning.

        :param scan_id: unique ID of this scan
        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "Scan commencement failed; component is in fault.",
            )
        else:
            self._scan_id = scan_id
            result = (ResultCode.OK, "Scan commencement completed OK")
        self._simulate_task_execution(
            task_callback, task_abort_event, result, scanning=True
        )

    @check_on
    def end_scan(
        self: FakeCspSubarrayComponent,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        End scanning.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        if self._state["fault"]:
            result = (
                ResultCode.FAILED,
                "End scan failed; component is in fault.",
            )
        else:
            self._scan_id = 0
            result = (ResultCode.OK, "End scan completed OK")
        self._simulate_task_execution(
            task_callback, task_abort_event, result, scanning=False
        )

    @check_on
    def simulate_scan_stopped(self: FakeCspSubarrayComponent) -> None:
        """Tell the component to simulate spontaneous stopping its scan."""
        self._update_state(scanning=False)

    @check_on
    def simulate_obsfault(self: FakeCspSubarrayComponent, obsfault: bool) -> None:
        """
        Tell the component to simulate (or stop simulating) an obsfault.

        :param obsfault: whether an obsfault has occurred
        """
        self._update_state(obsfault=obsfault)

    @check_on
    def obsreset(
        self: FakeCspSubarrayComponent,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Reset an observation that has faulted or been aborted.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        self._scan_id = 0
        self._config_id = ""
        result = (ResultCode.OK, "Obs reset completed OK")
        self._simulate_task_execution(
            task_callback,
            task_abort_event,
            result,
            obsfault=False,
            scanning=False,
            configured=False,
        )

    @check_on
    def restart(
        self: FakeCspSubarrayComponent,
        task_callback: Optional[Callable],
        task_abort_event: threading.Event,
    ) -> None:
        """
        Restart the component after it has faulted or been aborted.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param task_abort_event: a threading.Event that can be checked
            for whether this task has been aborted.
        """
        self._scan_id = 0
        self._config_id = ""
        result = (ResultCode.OK, "Restart completed OK")
        self._simulate_task_execution(
            task_callback,
            task_abort_event,
            result,
            obsfault=False,
            scanning=False,
            configured=False,
            resourced=False,
        )


# pylint: disable-next=abstract-method,too-many-public-methods
class ReferenceCspSubarrayComponentManager(
    GenericBaseComponentManager[FakeCspSubarrayComponent],
    CspSubarrayComponentManager,
):
    """
    A component manager for SKA CSP subelement observation Tango devices.

    The current implementation is intended to
    * illustrate the model
    * enable testing of the base classes

    It should not generally be used in concrete devices; instead, write
    a subclass specific to the component managed by the device.
    """

    def __init__(
        self: ReferenceCspSubarrayComponentManager,
        logger: logging.Logger,
        communication_state_callback: CommunicationStatusCallbackType,
        component_state_callback: Callable[[], None],
        _component: FakeCspSubarrayComponent | None = None,
    ) -> None:
        """
        Initialise a new ReferenceCspSubarrayComponentManager instance.

        :param logger: the logger for this component manager to log with
        :param communication_state_callback: callback to be called when
            the state of communications with the component changes
        :param component_state_callback: callback to be called when the
            state of the component changes
        """
        self._fail_communicate = False

        # self._component = _component or FakeCspSubarrayComponent()

        super().__init__(
            _component or FakeCspSubarrayComponent(),
            logger,
            communication_state_callback,
            component_state_callback,
            resourced=False,
            configured=False,
            scanning=False,
            obsfault=False,
        )

    def start_communicating(self: ReferenceCspSubarrayComponentManager) -> None:
        """Establish communication with the component, then start monitoring."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        if self.communication_state == CommunicationStatus.DISABLED:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        # The component would normally be an element of the system under control. In
        # order to establish communication with it, we might need, for example, to
        # establishing a network connection to the component, then start a polling loop
        # to continually poll over that connection.
        # But here, we're faking the component with an object, so all we need to do is
        # register some callbacks.
        # And in order to fake communications failure, we just return without
        # registering them.
        if self._fail_communicate:
            return

        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._component.set_state_change_callback(self._update_component_state)

    def stop_communicating(self: ReferenceCspSubarrayComponentManager) -> None:
        """Break off communication with the component."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return

        self._component.set_state_change_callback(None)
        self._update_component_state(power=PowerState.UNKNOWN, fault=None)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def simulate_communication_failure(
        self: ReferenceCspSubarrayComponentManager, fail_communicate: bool
    ) -> None:
        """
        Simulate (or stop simulating) a failure to communicate with the component.

        :param fail_communicate: whether the connection to the component
            is failing
        """
        self._fail_communicate = fail_communicate
        if (
            fail_communicate
            and self.communication_state == CommunicationStatus.ESTABLISHED
        ):
            self._component.set_state_change_callback(None)
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        elif (
            not fail_communicate
            and self.communication_state == CommunicationStatus.NOT_ESTABLISHED
        ):
            self._update_communication_state(CommunicationStatus.ESTABLISHED)
            self._component.set_state_change_callback(self._update_component_state)

    @property
    def power_state(self: ReferenceCspSubarrayComponentManager) -> PowerState:
        """
        Power mode of the component.

        This is just a bit of syntactic sugar for
        `self.component_state["power"]`.

        :return: the power mode of the component
        """
        return self._component_state["power"]

    @property
    def fault_state(self: ReferenceCspSubarrayComponentManager) -> bool:
        """
        Whether the component is currently faulting.

        :return: whether the component is faulting
        """
        return self._component_state["fault"]

    @check_communicating
    def off(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component off.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.off, task_callback=task_callback)

    @check_communicating
    def standby(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Put the component into low-power standby mode.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.standby, task_callback=task_callback)

    @check_communicating
    def on(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component on.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.on, task_callback=task_callback)

    @check_communicating
    def reset(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Reset the component (from fault state).

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.reset, task_callback=task_callback)

    @check_communicating
    def assign(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs: Any,
    ) -> tuple[TaskStatus, str]:
        """
        Assign resources to the component.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param kwargs: keyword arguments to the command

        :return: task status and a human-readable status message
        """
        resources = set(kwargs["resource_ids"])
        return self.submit_task(
            self._component.assign, (resources,), task_callback=task_callback
        )

    @check_communicating
    def release(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs: Any,
    ) -> tuple[TaskStatus, str]:
        """
        Release resources from the component.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param kwargs: keyword arguments to the command

        :return: task status and a human-readable status message
        """
        resources = set(kwargs["resource_ids"])
        return self.submit_task(
            self._component.release, (resources,), task_callback=task_callback
        )

    @check_communicating
    def release_all(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Release all resources.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(
            self._component.release_all, task_callback=task_callback
        )

    @check_communicating
    def configure(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs: Any,
    ) -> tuple[TaskStatus, str]:
        """
        Configure the component.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param kwargs: keyword arguments to the command

        :return: task status and a human-readable status message
        """
        # configuration = kwargs.get("config_id")
        return self.submit_task(
            self._component.configure,
            (kwargs,),
            task_callback=task_callback,
        )

    @check_communicating
    def deconfigure(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Deconfigure this component.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(
            self._component.deconfigure, task_callback=task_callback
        )

    @check_communicating
    def scan(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs: Any,
    ) -> tuple[TaskStatus, str]:
        """
        Start scanning.

        :param task_callback: a callback to be called whenever the
            status of this task changes.
        :param kwargs: keyword arguments to the command

        :return: task status and a human-readable status message
        """
        args = kwargs.get("scan_id")
        return self.submit_task(
            self._component.scan, (args,), task_callback=task_callback
        )

    @check_communicating
    def end_scan(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        End scanning.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.end_scan, task_callback=task_callback)

    @check_communicating
    def obsreset(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Deconfigure the component but do not release resources.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.obsreset, task_callback=task_callback)

    @check_communicating
    def restart(
        self: ReferenceCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Tell the component to restart.

        It will return to a state in which it is unconfigured and empty
        of assigned resources.

        :param task_callback: a callback to be called whenever the
            status of this task changes.

        :return: task status and a human-readable status message
        """
        return self.submit_task(self._component.restart, task_callback=task_callback)

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_communicating
    def config_id(self: ReferenceCspSubarrayComponentManager) -> str:
        """
        Return the configuration id.

        :return: the configuration id.
        """
        return self._component.config_id

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_on
    def scan_id(self: ReferenceCspSubarrayComponentManager) -> int:
        """
        Return the scan id.

        :return: the scan id.
        """
        return self._component.scan_id

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_communicating
    def last_scan_configuration(self: ReferenceCspSubarrayComponentManager) -> str:
        """
        Return the configuration id.

        :return: the configuration id.
        """
        return self._component.last_scan_configuration

    @property  # type: ignore[misc]  # mypy doesn't support decorated properties
    @check_communicating
    def assigned_resources(self: ReferenceCspSubarrayComponentManager) -> List[str]:
        """
        Return the list of assigned resources.

        :return: a list of strings with the FQDNs of the assigned resources
        """
        return self._component.assigned_resources
