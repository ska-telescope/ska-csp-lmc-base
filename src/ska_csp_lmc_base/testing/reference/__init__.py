"""
This subpackage implements reference component managers.

These are example component managers for use in testing, and as
explanatory material.
"""

__all__ = (
    "FakeCspObsComponent",
    "FakeCspSubarrayComponent",
    "ReferenceCspObsComponentManager",
    "ReferenceCspSubarrayComponentManager",
)

from .reference_obs_component_manager import (
    FakeCspObsComponent,
    ReferenceCspObsComponentManager,
)
from .reference_subarray_component_manager import (
    FakeCspSubarrayComponent,
    ReferenceCspSubarrayComponentManager,
)
