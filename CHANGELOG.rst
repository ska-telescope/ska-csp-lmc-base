###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project 

1.0.0
-----
- Updated package to use ska-tango-base 1.2.3

0.3.0
-----
- Updated package to use ska-tango-base 1.0.0

0.2.1
-----
- Building documentation use .readthedocs.yaml and poetry for requirements. 

0.2.0
-----
- Updated package to use ska-tango-base 0.19.1
- support pytango 9.4.2
- Remove XXXprogress TANGO attributes from Subarray since these are alreay
  implemented by the LRC attributes.
    
0.1.0
-----
- CSP sub-system classes moved to a separate repo from ska-tango-base v 0.17.0
