SKA CSP LMC base
================

This project contains base classes common to SKA CSP LMC projects.

That is, it contains Tango device base classes for all manner of CSP device:
spanning CSP LMC, CBF, PSS and PST, across both Mid and Low telescopes.
