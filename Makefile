include .make/base.mk

#########
# PYTHON
#########
PYTHON_LINE_LENGTH = 88
PYTHON_SWITCHES_FOR_BLACK =
PYTHON_SWITCHES_FOR_ISORT =
PYTHON_TEST_FILE = tests
PYTHON_VARS_AFTER_PYTEST = --forked
PYTHON_LINT_TARGET = src/ tests/

include .make/python.mk

python-post-lint:
	$(PYTHON_RUNNER) mypy --config-file mypy.ini $(PYTHON_LINT_TARGET)

.PHONY: python-post-lint


########
# DOCS
########
DOCS_SOURCEDIR=./docs/src
DOCS_SPHINXOPTS=-W --keep-going -n

include .make/docs.mk


##########
# PRIVATE
##########
-include PrivateRules.mak
