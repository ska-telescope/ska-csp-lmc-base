import os
import sys

########################
# GENERAL CONFIGURATION
########################
project = 'ska-csp-lmc-base'
copyright = '2023, SKAO'
author = 'SKAO'
release = '1.0.0'


templates_path = ['_templates']
exclude_patterns = []

html_theme = 'ska_ser_sphinx_theme'
html_static_path = ['_static']

html_context = {
    'gitlab-user': 'ska-telescope',
    'gitlab_repo': 'ska-csp-lmc-mid', #Repository name
    'gitlab_version': 'master',  #Version
}

def setup(app):
    """
    Initialise app.
    """
    app.add_css_file("css/custom.css")
    app.add_js_file("js/gitlab.js")

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinxcontrib.plantuml',
]

######################################
# CONFIGURATION FOR AUTODOC EXTENSION
######################################

autodoc_default_options = {
    'member-order': 'bysource',
}
autodoc_mock_imports = [
    "ska_tango_base",
    "tango",
]
autoclass_content = 'both'

add_module_names = False

sys.path.insert(0, os.path.abspath('../../src'))


################################
# CONFIGURATION FOR INTERSPHINX
################################

intersphinx_mapping = {
    "python": ("https://docs.python.org/3.10/", None),
    "ska-control-model": (
        "https://developer.skao.int/projects/ska-control-model/en/latest/",
        None,
    ),
    "tango": ("https://pytango.readthedocs.io/en/v9.4.2/", None),
}

nitpick_ignore = [
    # Version of sphinx in use here doesn't support type variables
    ("py:class", "ska_csp_lmc_base.controller_device.ComponentManagerT"),
    ("py:class", "ska_csp_lmc_base.obs.obs_device.ComponentManagerT"),
    ("py:class", "ska_csp_lmc_base.subarray.subarray_device.ComponentManagerT"),
    # ska_tango_base is mocked out so this can't be found
    ("py:exc", "StateModelError"),
]

#######################################
# CONFIGURATION FOR PLANTUML EXTENSION
#######################################

plantuml_syntax_error_image = True
