.. ska-csp-lmc-base documentation master file, created by
   sphinx-quickstart on Mon Feb 27 10:24:31 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ska-csp-lmc-base's documentation!
============================================

.. automodule:: ska_csp_lmc_base

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   API<api/index>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
