==============
CSP subpackage
==============

.. toctree::
  :caption: Subpackages
  :maxdepth: 2

  Obs subpackage<obs/index>
  Subarray subpackage<subarray/index>
  Testing subpackage<testing/index>

.. toctree::
  :caption: Other modules
  :maxdepth: 2

  Controller device<controller_device>
