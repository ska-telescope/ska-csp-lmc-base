===================
Subarray subpackage
===================

.. automodule:: ska_csp_lmc_base.subarray


.. toctree::

  Subarray Component Manager<component_manager>
  Subarray device<subarray_device>
