=====================
Obs component manager
=====================

.. automodule:: ska_csp_lmc_base.obs.component_manager
   :members:
