==============
Obs subpackage
==============

.. automodule:: ska_csp_lmc_base.obs


.. toctree::

  Obs State Model<obs_state_model>
  Obs Component Manager<component_manager>
  Obs device<obs_device>
