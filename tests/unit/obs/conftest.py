"""This module contains pytest test harness for testing the CSP obs device."""

from __future__ import annotations

import logging
from typing import Callable, Final, Generator

import pytest
import tango
from ska_control_model import AdminMode
from ska_tango_base.base import CommandTracker
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)

from ska_csp_lmc_base import CspSubElementObsDevice
from ska_csp_lmc_base.obs.obs_device import ComponentManagerT
from ska_csp_lmc_base.testing.reference import (
    FakeCspObsComponent,
    ReferenceCspObsComponentManager,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture(name="obsdevice_name", scope="session")
def obsdevice_name_fixture() -> str:
    """
    Return the name of the obsdevice Tango device.

    :return: the name of the obsdevice Tango device.
    """
    return "test/obsdevice/1"


@pytest.fixture(name="patched_obsdevice_class")
def patched_obsdevice_class_fixture() -> type[CspSubElementObsDevice]:
    """
    Return an obsdevice class patched with a non-abstract component manager.

    :return: an obsdevice device class patched with a non-abstract
        component manager.
    """

    class PatchedObsDevice(CspSubElementObsDevice):
        """CspSubElementObsDevice patched with a non-abstract component manager."""

        def create_component_manager(
            self: PatchedObsDevice,
        ) -> ReferenceCspObsComponentManager:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            component = FakeCspObsComponent()
            return ReferenceCspObsComponentManager(
                self.logger,
                self._communication_state_changed,
                self._component_state_changed,
                component,
            )

        class ConfigureScanCommand(CspSubElementObsDevice.ConfigureScanCommand):
            """A class for SKASubarray's Configure() command."""

            SCHEMA: Final = {
                # pylint: disable=line-too-long
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://skao.int/ska-tango-base/ReferenceSkaObsDevice_Configure.json",  # noqa: E501
                "title": "ska-tango-base ReferenceSkaObsDevice Configure schema",
                "description": "Schema for ska-tango-base ReferenceSkaObsDevice Configure command",  # noqa: E501
                "type": "object",
                "properties": {
                    "config_id": {
                        "description": "Configuration id",
                        "type": "string",
                    }
                },
                "required": [
                    "config_id",
                ],
            }

            def __init__(
                self: PatchedObsDevice.ConfigureScanCommand,
                command_tracker: CommandTracker,
                component_manager: ComponentManagerT,
                callback: Callable[[bool], None] | None = None,
                logger: logging.Logger | None = None,
            ) -> None:
                """
                Initialise a new instance.

                :param command_tracker: the device's command tracker
                :param component_manager: the device's component manager
                :param callback: an optional callback to be called when this
                    command starts and finishes.
                :param logger: a logger for this command to log with.
                """
                super().__init__(
                    command_tracker,
                    component_manager,
                    callback=callback,
                    logger=logger,
                    schema=self.SCHEMA,
                )

        class ScanCommand(CspSubElementObsDevice.ScanCommand):
            """A class for SKASubarray's Configure() command."""

            SCHEMA: Final = {
                # pylint: disable=line-too-long
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://skao.int/ska-tango-base/ReferenceSkaObsDevice_Scan.json",  # noqa: E501
                "title": "ska-tango-base ReferenceSkaObsDevice Configure schema",
                "description": "Schema for ska-tango-base ReferenceSkaObsDevice Scan command",  # noqa: E501
                "type": "object",
                "properties": {
                    "scan_id": {
                        "description": "Scan id",
                        "type": "integer",
                    }
                },
                "required": [
                    "scan_id",
                ],
            }

            def __init__(
                self: PatchedObsDevice.ScanCommand,
                command_tracker: CommandTracker,
                component_manager: ComponentManagerT,
                callback: Callable[[bool], None] | None = None,
                logger: logging.Logger | None = None,
            ) -> None:
                """
                Initialise a new instance.

                :param command_tracker: the device's command tracker
                :param component_manager: the device's component manager
                :param callback: an optional callback to be called when this
                    command starts and finishes.
                :param logger: a logger for this command to log with.
                """
                super().__init__(
                    command_tracker,
                    component_manager,
                    callback=callback,
                    logger=logger,
                    schema=self.SCHEMA,
                )

    return PatchedObsDevice


@pytest.fixture(name="device_id")
def device_id_fixture() -> float:
    """
    Return the device id.

    :return: the device id.
    """
    return 11


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    obsdevice_name: str,
    patched_obsdevice_class: type[CspSubElementObsDevice],
    device_id: int,
) -> Generator[TangoContextProtocol, None, None]:
    """
    Return a Tango harness against which to run tests of the deployment.

    :param obsdevice_name: the name of the CSP obsdevice Tango device
        under test
    :param patched_obsdevice_class: a subclass of CspSubElementObsDevice
        that has been patched with extra commands that mock system under
        control behaviours.
    :param device_id: the ID of the device

    :yields: a tango context.
    """
    context_manager = ThreadedTestTangoContextManager()
    context_manager.add_device(
        obsdevice_name,
        patched_obsdevice_class,
        DeviceID=device_id,
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="obsdevice")
def obsdevice_fixture(
    tango_harness: TangoContextProtocol,
    obsdevice_name: str,
) -> tango.DeviceProxy:
    """
    Fixture that returns the obsdevice Tango device under test.

    :param tango_harness: a test harness for Tango devices.
    :param obsdevice_name: name of the obsdevice Tango device.

    :returns: the obsdevice Tango device under test.
    """
    obsdevice = tango_harness.get_device(obsdevice_name)
    obsdevice.adminMode = AdminMode.ONLINE
    return obsdevice
