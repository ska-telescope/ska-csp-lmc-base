"""This module contains the tests for the CspSubelementObsDevice."""

from __future__ import annotations

import json
import logging
import re
from typing import Any, Callable

import pytest
import pytest_mock
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    ObsState,
    SimulationMode,
    TaskStatus,
    TestMode,
)
from ska_tango_base import SKAObsDevice
from ska_tango_base.base import JSONData
from ska_tango_base.long_running_commands_api import (
    LrcCallback,
    LrcSubscriptions,
    invoke_lrc,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevError, DevState
from tango.test_context import MultiDeviceTestContext

from ska_csp_lmc_base import CspSubElementObsDevice
from ska_csp_lmc_base.testing.reference import ReferenceCspObsComponentManager
from tests.unit.conftest import Helpers


# pylint: disable=too-many-arguments
class TestCspSubElementObsDevice:  # pylint: disable=too-many-public-methods
    """Test case for CSP SubElement ObsDevice class."""

    @pytest.fixture()
    def turn_on_device(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[], None]:
        """
        Turn on the device and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _turn_on_device() -> None:

            assert obsdevice.state() == DevState.OFF
            for attribute in [
                "state",
                "status",
                "obsState",
                "commandedObsState",
            ]:
                obsdevice.subscribe_event(
                    attribute,
                    tango.EventType.CHANGE_EVENT,
                    change_event_callbacks[attribute],
                )
                obsdevice.subscribe_event(
                    attribute, tango.EventType.CHANGE_EVENT, print
                )
            change_event_callbacks.assert_change_event("state", tango.DevState.OFF)
            change_event_callbacks.assert_change_event(
                "status", "The device is in OFF state."
            )
            change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)

            change_event_callbacks.assert_change_event(
                "commandedObsState", ObsState.IDLE
            )

            # Call command
            _ = invoke_lrc(successful_lrc_callback, obsdevice, "On")

            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                    "lrc_callback(progress=33)",
                ],
            )
            # Command is completed
            change_event_callbacks.assert_change_event("state", tango.DevState.ON)

            change_event_callbacks.assert_change_event(
                "status",
                "The device is in ON state.",
                # lookahead=2
            )
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(progress=66)",
                    "lrc_callback(result=[0, 'On command completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            assert obsdevice.state() == tango.DevState.ON

        return _turn_on_device

    @pytest.fixture()
    def configure_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        aborted_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[dict[str, str], bool], LrcSubscriptions]:
        """
        Configure the device and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param aborted_lrc_callback: callback fixture to use with invoke_lrc
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _configure_obsdevice(
            configuration_to_apply: dict[str, str], to_be_aborted: bool
        ) -> Any:
            """
            Configure the device and clear the queue attributes.

            :param configuration_to_apply: dict
            :param to_be_aborted: if command will be aborted while in progress
            :return: the executed Configure() command's unique ID
            """
            # Call command
            configure_command = invoke_lrc(
                aborted_lrc_callback if to_be_aborted else successful_lrc_callback,
                obsdevice,
                "ConfigureScan",
                (json.dumps(configuration_to_apply),),
            )

            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                ],
            )
            change_event_callbacks.assert_change_event(
                "obsState",
                ObsState.CONFIGURING,
                # lookahead=2
            )

            if to_be_aborted:
                return configure_command
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(progress=33)",
                    "lrc_callback(progress=66)",
                    "lrc_callback(result=[0, 'Configure completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            change_event_callbacks.assert_change_event(
                "obsState",
                ObsState.READY,  # lookahead=2
            )
            # Command is completed
            assert obsdevice.obsState == ObsState.READY
            return configure_command

        return _configure_obsdevice

    @pytest.fixture()
    def deconfigure_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[], None]:
        """
        Deconfigure the device and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _deconfigure_obsdevice() -> None:
            """Deconfigure the device and clear the queue attributes."""
            # Deconfigure (GoToIdle)
            _ = invoke_lrc(successful_lrc_callback, obsdevice, "GoToIdle")
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                    "lrc_callback(progress=33)",
                    "lrc_callback(progress=66)",
                    "lrc_callback(result=[0, 'Deconfigure completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
            assert obsdevice.obsState == obsdevice.commandedObsState

        return _deconfigure_obsdevice

    @pytest.fixture()
    def scan_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        aborted_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[dict[str, int], bool], LrcSubscriptions]:
        """
        Deconfigure the device and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param aborted_lrc_callback: callback fixture to use with abort
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _scan_obsdevice(scan_to_apply: dict[str, int], to_be_aborted: bool) -> Any:
            """
            Start a Scan.

            :param scan_to_apply: dict with the scan information
            :param to_be_aborted: if command will be aborted while in progress
            :return: the executed Scan() command's unique ID
            """
            # Call command
            scan_command = invoke_lrc(
                aborted_lrc_callback if to_be_aborted else successful_lrc_callback,
                obsdevice,
                "Scan",
                (json.dumps(scan_to_apply),),
            )

            change_event_callbacks.assert_change_event("obsState", ObsState.SCANNING)
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                    "lrc_callback(progress=33)",
                    "lrc_callback(progress=66)",
                    "lrc_callback(result="
                    f"[0, 'Scan {scan_to_apply['scan_id']}"
                    " commencement completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            return scan_command

        return _scan_obsdevice

    @pytest.fixture()
    def endscan_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[], None]:
        """
        Deconfigure the device and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _endscan_obsdevice() -> None:
            # End scan
            _ = invoke_lrc(successful_lrc_callback, obsdevice, "EndScan")
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                    "lrc_callback(progress=33)",
                    "lrc_callback(progress=66)",
                    "lrc_callback(result=[0, 'End scan completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            change_event_callbacks.assert_change_event("obsState", ObsState.READY)

        return _endscan_obsdevice

    @pytest.fixture()
    def abort_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        logger: logging.Logger,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[LrcSubscriptions], None]:
        """
        Abort the given command in progress and clear the queue attributes.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param logger: test logger
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def abort_callback(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: JSONData | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            if progress is not None:
                logger.info(f"abort_callback(progress={progress})")
            if result is not None:
                logger.info(f"abort_callback(result={result})")
            if status is not None:
                logger.info(f"abort_callback(status={status.name})")
            if error is not None:
                logger.error(f"abort_callback(error={error})")
            if kwargs:
                logger.error(f"abort_callback(kwargs={kwargs})")

        def _abort_obsdevice(lrc: LrcSubscriptions) -> None:
            """
            Abort the given command in progress and clear the queue attributes.

            :param lrc: of command in progress to abort
            """
            _ = invoke_lrc(abort_callback, obsdevice, "Abort")
            change_event_callbacks.assert_change_event("obsState", ObsState.ABORTING)
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "abort_callback(status=STAGING)",
                    "abort_callback(status=IN_PROGRESS)",
                    "abort_callback(result=[0, 'Abort completed OK'])",
                    "abort_callback(status=COMPLETED)",
                    "lrc_callback(result=[7, 'Command has been aborted'])",
                    "lrc_callback(status=ABORTED)",
                ],
            )
            change_event_callbacks.assert_change_event("obsState", ObsState.ABORTED)
            change_event_callbacks.assert_not_called()
            del lrc

        return _abort_obsdevice

    @pytest.fixture()
    def reset_obsdevice(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
        successful_lrc_callback: LrcCallback,
        aborted_lrc_callback: LrcCallback,
        caplog: pytest.LogCaptureFixture,
    ) -> Callable[[ObsState, bool], LrcSubscriptions]:
        """
        Reset the device.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event callbacks
        :param successful_lrc_callback: callback fixture to use with invoke_lrc
        :param aborted_lrc_callback: callback fixture to use with invoke_lrc
        :param caplog: pytest LogCaptureFixture
        :return: Callable helper function
        """

        def _reset_obsdevice(
            expected_obs_state: ObsState, to_be_aborted: bool
        ) -> LrcSubscriptions:
            """
            Reset the device and clear the queue attributes.

            :param expected_obs_state: the expected obsState after ObsReset completed
            :param to_be_aborted: if command will be aborted while in progress
            :return: the executed ObsReset() command's unique ID
            """
            # Call command
            reset_command = invoke_lrc(
                aborted_lrc_callback if to_be_aborted else successful_lrc_callback,
                obsdevice,
                "ObsReset",
            )
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(status=STAGING)",
                    "lrc_callback(status=QUEUED)",
                    "lrc_callback(status=IN_PROGRESS)",
                ],
            )
            change_event_callbacks.assert_change_event("obsState", ObsState.RESETTING)
            if to_be_aborted:
                return reset_command
            Helpers.assert_expected_logs(
                caplog,
                [  # Log messages must be in this exact order
                    "lrc_callback(progress=33)",
                    "lrc_callback(progress=66)",
                    "lrc_callback(result=[0, 'Obs reset completed OK'])",
                    "lrc_callback(status=COMPLETED)",
                ],
            )
            # Command is completed
            change_event_callbacks.assert_change_event("obsState", expected_obs_state)
            return reset_command

        return _reset_obsdevice

    def test_state(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for State.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.state() == tango.DevState.OFF

    def test_status(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for Status.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.Status() == "The device is in OFF state."

    def test_get_version_info(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for GetVersionInfo.

        :param obsdevice: a proxy to the device under test
        """
        version_pattern = (
            f"{obsdevice.info().dev_class}, ska_tango_base, "
            "[0-9]+.[0-9]+.[0-9]+, A set of generic base devices for SKA Telescope."
        )
        version_info = obsdevice.GetVersionInfo()
        assert len(version_info) == 1
        assert re.match(version_pattern, version_info[0])

    def test_build_state(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for buildState.

        :param obsdevice: a proxy to the device under test
        """
        build_pattern = re.compile(
            r"ska_tango_base, [0-9]+.[0-9]+.[0-9]+, "
            r"A set of generic base devices for SKA Telescope"
        )
        assert (re.match(build_pattern, obsdevice.buildState)) is not None

    def test_version_id(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for versionId.

        :param obsdevice: a proxy to the device under test
        """
        version_id_pattern = re.compile(r"[0-9]+.[0-9]+.[0-9]+")
        assert (re.match(version_id_pattern, obsdevice.versionId)) is not None

    def test_health_state(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for healthState.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.healthState == HealthState.UNKNOWN

    def test_admin_mode(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for adminMode.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.adminMode == AdminMode.ONLINE

    def test_control_mode(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for controlMode.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.controlMode == ControlMode.REMOTE

    def test_simulation_mode(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for simulationMode.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.simulationMode == SimulationMode.FALSE

    def test_test_mode(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for testMode.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.testMode == TestMode.NONE

    def test_scan_id(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for scanID.

        :param obsdevice: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support
        """
        assert obsdevice.state() == tango.DevState.OFF

        obsdevice.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.OFF)

        obsdevice.On()

        change_event_callbacks.assert_change_event("state", tango.DevState.ON)
        assert obsdevice.state() == tango.DevState.ON

        assert obsdevice.scanID == 0

    def test_device_id(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        device_id: int,
    ) -> None:
        """
        Test for deviceID.

        :param obsdevice: a proxy to the device under test
        :param device_id: the ID of the device
        """
        assert obsdevice.deviceID == device_id

    def test_sdp_destination_addresses(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for sdpDestinationAddresses.

        :param obsdevice: a proxy to the device under test
        """
        addresses_dict: dict[str, list] = {
            "outputHost": [],
            "outputMac": [],
            "outputPort": [],
        }
        assert obsdevice.sdpDestinationAddresses == json.dumps(addresses_dict)

    def test_sdplink_activity(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for sdpLinkActive.

        :param obsdevice: a proxy to the device under test
        """
        assert [i is False for i in obsdevice.sdpLinkActive]

    def test_sdplink_capacity(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for sdpLinkCapacity.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.sdpLinkCapacity == 0

    def test_health_failure_message(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for healthFailureMessage.

        :param obsdevice: a proxy to the device under test
        """
        assert obsdevice.healthFailureMessage == ""

    def test_configurescan_and_gotoidle(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
        configure_obsdevice: Callable[[dict[str, str], bool], LrcSubscriptions],
        deconfigure_obsdevice: Callable[[], None],
    ) -> None:
        """
        Test for ConfigureScan and GoToIdle.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        :param configure_obsdevice: helper function
        :param deconfigure_obsdevice: helper function
        """
        turn_on_device()
        assert obsdevice.configurationId == ""

        config_id = "sbi-mvp01-20200325-00002"

        configure_obsdevice({"config_id": config_id}, False)

        assert obsdevice.configurationId == config_id

        # Call deconfigure
        deconfigure_obsdevice()

        assert obsdevice.configurationId == ""

    def test_configurescan_when_in_wrong_state(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for ConfigureScan when the device is in wrong state.

        :param obsdevice: a proxy to the device under test
        """
        # The device in in OFF/IDLE state, not valid to invoke ConfigureScan.

        with pytest.raises(tango.DevFailed, match="Component is not powered ON"):
            obsdevice.ConfigureScan(
                '{"config_id":"sbi-mvp01-20200325-00002"}'
            )  # noqa: FS003

    def test_configurescan_with_wrong_input_args(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
    ) -> None:
        """
        Test ConfigureScan's handling of wrong input arguments.

        Specifically, test when input argument specifies a wrong json
        configuration and the device is in IDLE state.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        """
        assert obsdevice.state() == tango.DevState.OFF
        turn_on_device()

        # wrong configurationID key
        wrong_configuration = '{"subid":"sbi-mvp01-20200325-00002"}'  # noqa: FS003
        with pytest.raises(
            tango.DevFailed, match=r"'config_id' is a required property"
        ):
            obsdevice.ConfigureScan(wrong_configuration)

    def test_configurescan_with_json_syntax_error(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
    ) -> None:
        """
        Test for ConfigureScan when syntax error in json configuration.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        """
        turn_on_device()
        with pytest.raises(
            tango.DevFailed, match="jsonschema.exceptions.ValidationError: 'config_id'"
        ):
            obsdevice.ConfigureScan('{"foo": 1}')  # noqa: FS003

            assert obsdevice.obsState == ObsState.IDLE

    def test_gotoidle_when_in_wrong_state(
        self: TestCspSubElementObsDevice, obsdevice: tango.DeviceProxy
    ) -> None:
        """
        Test for GoToIdle when the device is in wrong state.

        :param obsdevice: a proxy to the device under test
        """
        # The device in in OFF/IDLE state, not valid to invoke GoToIdle.
        with pytest.raises(
            tango.DevFailed,
            match="GoToIdle command not permitted in observation state IDLE",
        ):
            obsdevice.GoToIdle()

    def test_scan_and_endscan(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
        configure_obsdevice: Callable[[dict[str, str], bool], LrcSubscriptions],
        scan_obsdevice: Callable[[dict[str, int], bool], LrcSubscriptions],
        endscan_obsdevice: Callable[[], None],
    ) -> None:
        """
        Test for Scan and EndScan.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        :param configure_obsdevice: helper function
        :param scan_obsdevice: helper function
        :param endscan_obsdevice: helper function
        """
        assert obsdevice.state() == tango.DevState.OFF

        turn_on_device()
        assert obsdevice.scanId == 0
        config_id = "sbi-mvp01-20200325-00002"

        configure_obsdevice({"config_id": config_id}, False)

        assert obsdevice.configurationId == config_id
        scan_obsdevice({"scan_id": 1}, False)

        assert obsdevice.scanId == 1

        # test end_scan
        endscan_obsdevice()
        assert obsdevice.scanId == 0

    def test_scan_when_in_wrong_state(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
    ) -> None:
        """
        Test for Scan when the device is in wrong state.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        """
        # Set the device in ON/IDLE state
        assert obsdevice.state() == tango.DevState.OFF

        turn_on_device()

        with pytest.raises(
            tango.DevFailed,
            match="Scan command not permitted in observation state IDLE",
        ):
            obsdevice.Scan('{"scan_id": 12}')

    def test_scan_with_wrong_argument(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
        configure_obsdevice: Callable[[dict[str, str], bool], LrcSubscriptions],
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for Scan when a wrong input argument is passed.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        :param configure_obsdevice: helper function
        :param change_event_callbacks: dictionary of mock change event
                callbacks
        """
        # Set the device in ON/IDLE state
        assert obsdevice.state() == tango.DevState.OFF

        turn_on_device()

        config_id = "sbi-mvp01-20250109-00001"

        configure_obsdevice({"config_id": config_id}, False)

        with pytest.raises(
            tango.DevFailed, match=r"ValidationError: 12 is not of type 'object'"
        ):
            obsdevice.Scan("12")

        change_event_callbacks.assert_not_called()
        assert obsdevice.obsState == ObsState.READY

    def test_endscan_when_in_wrong_state(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
        configure_obsdevice: Callable[[dict[str, str], bool], LrcSubscriptions],
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for EndScan when the device is in wrong state.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        :param configure_obsdevice: helper function
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support
        """
        # Set the device in ON/READY state
        assert obsdevice.state() == tango.DevState.OFF

        turn_on_device()

        config_id = "sbi-mvp01-2020250109-00003"

        configure_obsdevice({"config_id": config_id}, False)

        with pytest.raises(
            tango.DevFailed,
            match="EndScan command not permitted in observation state READY",
        ):
            obsdevice.EndScan()
        change_event_callbacks.assert_not_called()
        assert obsdevice.obsState == ObsState.READY

    @pytest.mark.thisone
    def test_abort_and_obsreset(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
        configure_obsdevice: Callable[[dict[str, str], bool], LrcSubscriptions],
        abort_obsdevice: Callable[[LrcSubscriptions], None],
        reset_obsdevice: Callable[[ObsState, bool], LrcSubscriptions],
    ) -> None:
        """
        Test for Abort and ObsReset.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        :param configure_obsdevice: helper function
        :param abort_obsdevice: helper function
        :param reset_obsdevice: helper function
        """
        assert obsdevice.state() == tango.DevState.OFF

        turn_on_device()

        # Start configuring but then abort
        configure_command = configure_obsdevice(
            {"config_id": "sbi-mvp01-20200325-00002"}, True
        )
        abort_obsdevice(configure_command)
        reset_obsdevice(ObsState.IDLE, False)

    def test_obsreset_when_in_wrong_state(
        self: TestCspSubElementObsDevice,
        obsdevice: tango.DeviceProxy,
        turn_on_device: Callable[[], None],
    ) -> None:
        """
        Test for ObsReset when the device is in wrong state.

        :param obsdevice: a proxy to the device under test
        :param turn_on_device: helper function
        """
        # Set the device in ON/IDLE state
        turn_on_device()

        with pytest.raises(
            tango.DevFailed,
            match="ObsReset command not permitted in observation state IDLE",
        ):
            obsdevice.ObsReset()


@pytest.mark.forked
def test_multiple_devices_in_same_process(
    mocker: pytest_mock.MockerFixture,
    patched_obsdevice_class: type[CspSubElementObsDevice],
) -> None:
    """
    Test that we can run this device with other devices in a single process.

    :param mocker: pytest fixture that wraps :py:mod:`unittest.mock`.
    :param patched_obsdevice_class: a subclass of
        CspSubElementObsDevice been patched with extra commands that
        mock system under control behaviours.
    """
    # Patch abstract method/s; it doesn't matter what we patch them with, so long as
    # they don't raise NotImplementedError.
    component_manager = ReferenceCspObsComponentManager(
        mocker.MagicMock(), mocker.MagicMock(), mocker.MagicMock()
    )

    mocker.patch(
        "ska_tango_base.obs.obs_device.SKAObsDevice.create_component_manager",
        return_value=component_manager,
    )

    devices_info = (
        {"class": patched_obsdevice_class, "devices": [{"name": "test/se/1"}]},
        {"class": SKAObsDevice, "devices": [{"name": "test/obsdevice/1"}]},
    )

    with MultiDeviceTestContext(devices_info, process=False) as context:
        proxy1 = context.get_device("test/se/1")
        proxy2 = context.get_device("test/obsdevice/1")
        assert proxy1.state() == tango.DevState.DISABLE
        assert proxy2.state() == tango.DevState.DISABLE
