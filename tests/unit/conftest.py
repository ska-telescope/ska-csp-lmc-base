"""This module defines elements of the pytest test harness shared by all tests."""

from __future__ import annotations

import logging
import time
from typing import Any, Generator

import pytest
from ska_control_model import ResultCode, TaskStatus
from ska_tango_base.base import JSONData
from ska_tango_base.long_running_commands_api import LrcCallback
from ska_tango_testing.mock import MockCallableGroup
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevError

module_logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "off_task",
        "standby_task",
    )


@pytest.fixture(name="logger")
def logger_fixture() -> logging.Logger:
    """
    Return default logger.

    :return: default logger for tests.
    """
    logger = logging.getLogger("Test logger")
    logger.setLevel(logging.INFO)
    return logger


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "obsState",
        "commandedObsState",
        "state",
        "status",
        "lrcQueue",
        "lrcExecuting",
        "lrcFinished",
        "longRunningCommandProgress",
        "longRunningCommandResult",
        "longRunningCommandStatus",
        "longRunningCommandInProgress",
        "longRunningCommandsInQueue",
        "longRunningCommandIDsInQueue",
    )


@pytest.fixture(name="successful_lrc_callback")
def successful_lrc_callback_fixture(
    logger: logging.Logger,
) -> Generator[LrcCallback, None, None]:
    """
    Use this callback with invoke_lrc when the LRC should complete successfully.

    :yields: successful_lrc_callback function.
    :raises AssertionError: if unexpected status, progress, result or error is received.
    """  # noqa DAR401,DAR402
    assert_errors: list[AssertionError] = []

    def _successful_lrc_callback(
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        try:

            if progress is not None:
                logger.info(f"lrc_callback(progress={progress})")
                assert progress in [33, 66], f"Unexpected progress: {progress}"
            if result is not None:
                logger.info(f"lrc_callback(result={result})")
                assert isinstance(result, list) and result[0] == ResultCode.OK, {
                    f"Unexpected result: {result}"
                }
            if status is not None:
                logger.info(f"lrc_callback(status={status.name})")
                assert status in [
                    TaskStatus.STAGING,
                    TaskStatus.QUEUED,
                    TaskStatus.IN_PROGRESS,
                    TaskStatus.COMPLETED,
                ], f"Unexpected status: {status.name}"
            if error is not None:
                logger.error(f"lrc_callback(error={error})")
                assert False, f"Received {error}"
            if kwargs:
                logger.error(f"lrc_callback(kwargs={kwargs})")
        except AssertionError as exc:
            assert_errors.append(exc)

    yield _successful_lrc_callback
    if assert_errors:
        raise assert_errors[0]


@pytest.fixture(name="aborted_lrc_callback")
def aborted_lrc_callback_fixture(
    logger: logging.Logger,
) -> Generator[LrcCallback, None, None]:
    """
    Use this callback with invoke_lrc when the LRC should be aborted after starting.

    :yields: aborted_lrc_callback function.
    :raises AssertionError: if unexpected status, progress, result or error is received.
    """  # noqa DAR401,DAR402
    assert_errors: list[AssertionError] = []

    def _aborted_lrc_callback(
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        try:
            if progress is not None:
                logger.info(f"lrc_callback(progress={progress})")
                assert False, f"Unexpected progress: {progress}"
            if result is not None:
                logger.info(f"lrc_callback(result={result})")
                assert isinstance(result, list) and result[0] == ResultCode.ABORTED, {
                    f"Unexpected result: {result}"
                }
            if status is not None:
                logger.info(f"lrc_callback(status={status.name})")
                assert status in [
                    TaskStatus.STAGING,
                    TaskStatus.QUEUED,
                    TaskStatus.IN_PROGRESS,
                    TaskStatus.ABORTED,
                ], f"Unexpected status: {status.name}"
            if error is not None:
                logger.error(f"lrc_callback(error={error})")
                assert False, f"Received {error}"
            if kwargs:
                logger.error(f"lrc_callback(kwargs={kwargs})")
        except AssertionError as exc:
            assert_errors.append(exc)

    yield _aborted_lrc_callback
    if assert_errors:
        raise assert_errors[0]


class Helpers:
    """Static helper functions for tests."""

    @staticmethod
    def assert_lrcstatus_change_event_staging_queued_in_progress(
        change_event_callbacks: MockTangoEventCallbackGroup, command: Any
    ) -> None:
        """
        Assert the longRunningCommandStatus attribute change event multiple times.

        :param change_event_callbacks: dictionary of mock change event callbacks
        :param command: name/id of command to assert change events
        """
        for status in ["STAGING", "QUEUED", "IN_PROGRESS"]:
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command, status)
            )

    @staticmethod
    def assert_expected_logs(
        caplog: pytest.LogCaptureFixture,
        expected_logs: list[str],
        timeout: int = 2,
    ) -> None:
        """
        Assert the expected log messages are in the captured logs.

        The expected list of log messages must appear in the records in the same order.
        The captured logs are cleared before returning for subsequent assertions.

        :param caplog: pytest log capture fixture.
        :param expected_logs: to assert are in the log capture fixture.
        :param timeout: time to wait for the last log message to appear, default 2 secs.
        """
        start_time = time.time()
        while time.time() - start_time < timeout:
            if expected_logs[-1] in caplog.text:
                break
        else:
            pytest.fail(f"'{expected_logs}' not found in logs within {timeout} seconds")
        test_logs = [
            record.message for record in caplog.records if "_callback" in record.message
        ]
        assert test_logs == expected_logs
        caplog.clear()
