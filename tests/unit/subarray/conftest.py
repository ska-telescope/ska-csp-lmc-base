"""This module contains pytest test harness for testing the CSP subarray."""

from __future__ import annotations

import logging
from typing import Callable, Final, Generator

import pytest
import tango
from ska_control_model import AdminMode
from ska_tango_base.base import CommandTracker
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)

from ska_csp_lmc_base import CspSubElementSubarray
from ska_csp_lmc_base.subarray.subarray_device import ComponentManagerT
from ska_csp_lmc_base.testing.reference import (
    FakeCspSubarrayComponent,
    ReferenceCspSubarrayComponentManager,
)


@pytest.fixture(name="subarray_name", scope="session")
def subarray_name_fixture() -> str:
    """
    Return the name of the subarray Tango device under test.

    :return: the name of the subarray Tango device.
    """
    return "test/subarray/01"


@pytest.fixture(name="patched_subarray_class")
def patched_subarray_class_fixture() -> type[CspSubElementSubarray]:
    """
    Return a subarray device class patched with a non-abstract component manager.

    :return: a subarray device class patched with a non-abstract
        component manager.
    """

    class PatchedSubarray(CspSubElementSubarray):  # pylint: disable=too-many-ancestors
        """CspSubElementSubarray patched with a non-abstract component manager."""

        def create_component_manager(
            self: PatchedSubarray,
        ) -> ReferenceCspSubarrayComponentManager:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            component = FakeCspSubarrayComponent()
            return ReferenceCspSubarrayComponentManager(
                self.logger,
                self._communication_state_changed,
                self._component_state_changed,
                component,
            )

        class ConfigureScanCommand(CspSubElementSubarray.ConfigureScanCommand):
            """A class for SKASubarray's Configure() command."""

            SCHEMA: Final = {
                # pylint: disable=line-too-long
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://skao.int/ska-tango-base/ReferenceSkaSubarray_Configure.json",  # noqa: E501
                "title": "ska-tango-base ReferenceSkaSubarray Configure schema",
                "description": "Schema for ska-tango-base ReferenceSkaSubarray Configure command",  # noqa: E501
                "type": "object",
                "properties": {
                    "config_id": {
                        "description": "Configuration id",
                        "type": "string",
                    }
                },
                "required": [
                    "config_id",
                ],
            }

            def __init__(
                self: PatchedSubarray.ConfigureScanCommand,
                command_tracker: CommandTracker,
                component_manager: ComponentManagerT,
                callback: Callable[[bool], None] | None = None,
                logger: logging.Logger | None = None,
            ) -> None:
                """
                Initialise a new instance.

                :param command_tracker: the device's command tracker
                :param component_manager: the device's component manager
                :param callback: an optional callback to be called when this
                    command starts and finishes.
                :param logger: a logger for this command to log with.
                """
                super().__init__(
                    command_tracker,
                    component_manager,
                    callback=callback,
                    logger=logger,
                    schema=self.SCHEMA,
                )

    return PatchedSubarray


@pytest.fixture(name="capability_types")
def capability_types_fixture() -> list[str]:
    """
    Return the subarray's capability types.

    :return: the capability types.
    """
    return ["id"]


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    subarray_name: str,
    patched_subarray_class: type[CspSubElementSubarray],
    capability_types: list[str],
) -> Generator[TangoContextProtocol, None, None]:
    """
    Return a Tango harness against which to run tests of the deployment.

    :param subarray_name: the name of the subarray Tango device
    :param patched_subarray_class: a subclass of CSP subarray
        that has been patched with extra commands that mock system under
        control behaviours.
    :param capability_types: the capability types for this subarray

    :yields: a tango context.
    """
    context_manager = ThreadedTestTangoContextManager()
    context_manager.add_device(
        subarray_name,
        patched_subarray_class,
        CapabilityTypes=capability_types,
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="subarray")
def subarray_fixture(
    tango_harness: TangoContextProtocol,
    subarray_name: str,
) -> tango.DeviceProxy:
    """
    Fixture that returns the subarray Tango device under test.

    :param tango_harness: a test harness for Tango devices.
    :param subarray_name: name of the subarray Tango device.

    :returns: the tile Tango device under test.
    """
    subarray = tango_harness.get_device(subarray_name)
    subarray.adminMode = AdminMode.ONLINE
    return subarray
