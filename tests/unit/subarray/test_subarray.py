# pylint: disable=invalid-name
"""This module tests the :py:mod:``ska_tango_base.csp.subarray_device`` module."""
from __future__ import annotations

import json
import re

import pytest
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    ObsState,
    ResultCode,
    SimulationMode,
    TestMode,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_csp_lmc_base.testing.reference import FakeCspSubarrayComponent


@pytest.mark.skip(reason="Not implemented")
def test_properties(  # pylint: disable=unused-argument
    subarray: tango.DeviceProxy,
) -> None:
    """
    Test the device properties.

    :param subarray: a proxy to the device under test
    """


def test_State(subarray: tango.DeviceProxy) -> None:
    """
    Test for State.

    :param subarray: a proxy to the device under test
    """
    assert subarray.state() == tango.DevState.OFF


def test_Status(subarray: tango.DeviceProxy) -> None:
    """
    Test for Status.

    :param subarray: a proxy to the device under test
    """
    assert subarray.Status() == "The device is in OFF state."


def test_GetVersionInfo(subarray: tango.DeviceProxy) -> None:
    """
    Test for GetVersionInfo.

    :param subarray: a proxy to the device under test
    """
    version_pattern = (
        f"{subarray.info().dev_class}, ska_tango_base, "
        "[0-9]+.[0-9]+.[0-9]+, A set of generic base devices for SKA Telescope."
    )
    version_info = subarray.GetVersionInfo()
    assert len(version_info) == 1
    assert re.match(version_pattern, version_info[0])


def test_buildState(subarray: tango.DeviceProxy) -> None:
    """
    Test for buildState.

    :param subarray: a proxy to the device under test
    """
    build_pattern = re.compile(
        r"ska_tango_base, [0-9]+.[0-9]+.[0-9]+, "
        r"A set of generic base devices for SKA Telescope"
    )
    assert (re.match(build_pattern, subarray.buildState)) is not None


def test_versionId(subarray: tango.DeviceProxy) -> None:
    """
    Test for versionId.

    :param subarray: a proxy to the device under test
    """
    version_id_pattern = re.compile(r"[0-9]+.[0-9]+.[0-9]+")
    assert (re.match(version_id_pattern, subarray.versionId)) is not None


def test_healthState(subarray: tango.DeviceProxy) -> None:
    """
    Test for healthState.

    :param subarray: a proxy to the device under test
    """
    assert subarray.healthState == HealthState.UNKNOWN


def test_adminMode(subarray: tango.DeviceProxy) -> None:
    """
    Test for adminMode.

    :param subarray: a proxy to the device under test
    """
    assert subarray.adminMode == AdminMode.ONLINE


def test_controlMode(subarray: tango.DeviceProxy) -> None:
    """
    Test for controlMode.

    :param subarray: a proxy to the device under test
    """
    assert subarray.controlMode == ControlMode.REMOTE


def test_simulationMode(subarray: tango.DeviceProxy) -> None:
    """
    Test for simulationMode.

    :param subarray: a proxy to the device under test
    """
    assert subarray.simulationMode == SimulationMode.FALSE


def test_testMode(subarray: tango.DeviceProxy) -> None:
    """
    Test for testMode.

    :param subarray: a proxy to the device under test
    """
    assert subarray.testMode == TestMode.NONE


def test_scanID(
    subarray: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test for scanID.

    :param subarray: a proxy to the device under test
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    """
    assert subarray.state() == tango.DevState.OFF

    subarray.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks.assert_change_event("state", tango.DevState.OFF)

    subarray.On()

    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    assert subarray.state() == tango.DevState.ON

    assert subarray.scanID == 0


def test_sdpDestinationAddresses(subarray: tango.DeviceProxy) -> None:
    """
    Test for sdpDestinationAddresses.

    :param subarray: a proxy to the device under test
    """
    addresses_dict: dict[str, list] = {
        "outputHost": [],
        "outputMac": [],
        "outputPort": [],
    }
    subarray.sdpDestinationAddresses = json.dumps(addresses_dict)
    assert subarray.sdpDestinationAddresses == json.dumps(addresses_dict)


def test_sdpLinkActivity(subarray: tango.DeviceProxy) -> None:
    """
    Test for sdpLinkActive.

    :param subarray: a proxy to the device under test
    """
    actual = subarray.sdpLinkActive
    n_links = len(actual)
    expected = [False for i in range(0, n_links)]
    assert all(a == b for a, b in zip(actual, expected))


def test_outputDataRateToSdp(subarray: tango.DeviceProxy) -> None:
    """
    Test for outputDataRateToSdp.

    :param subarray: a proxy to the device under test
    """
    assert subarray.outputDataRateToSdp == 0


def test_listOfDevicesCompletedTasks(subarray: tango.DeviceProxy) -> None:
    """
    Test for listOfDevicesCompletedTasks.

    :param subarray: a proxy to the device under test
    """
    attr_value_as_dict = json.loads(subarray.listOfDevicesCompletedTasks)
    assert not bool(attr_value_as_dict)


def test_assignResourcesMaximumDuration(subarray: tango.DeviceProxy) -> None:
    """
    Test for assignResourcesMaximumDuration.

    :param subarray: a proxy to the device under test
    """
    subarray.assignResourcesMaximumDuration = 5
    assert subarray.assignResourcesMaximumDuration == 5


def test_configureScanMeasuredDuration(subarray: tango.DeviceProxy) -> None:
    """
    Test for configureScanMeasuredDuration.

    :param subarray: a proxy to the device under test
    """
    assert subarray.configureScanMeasuredDuration == 0


def test_configurationProgress(subarray: tango.DeviceProxy) -> None:
    """
    Test for configurationProgress.

    :param subarray: a proxy to the device under test
    """
    assert subarray.configurationProgress == 0


def test_assignResourcesMeasuredDuration(subarray: tango.DeviceProxy) -> None:
    """
    Test for assignResourcesMeasuredDuration.

    :param subarray: a proxy to the device under test
    """
    assert subarray.assignResourcesMeasuredDuration == 0


def test_releaseResourcesMaximumDuration(subarray: tango.DeviceProxy) -> None:
    """
    Test for releaseResourcesMaximumDuration.

    :param subarray: a proxy to the device under test
    """
    subarray.releaseResourcesMaximumDuration = 5
    assert subarray.releaseResourcesMaximumDuration == 5


def test_releaseResourcesMeasuredDuration(subarray: tango.DeviceProxy) -> None:
    """
    Test for releaseResourcesMeasuredDuration.

    :param subarray: a proxy to the device under test
    """
    assert subarray.releaseResourcesMeasuredDuration == 0


def test_configureScanTimeoutExpiredFlag(subarray: tango.DeviceProxy) -> None:
    """
    Test for timeoutExpiredFlag.

    :param subarray: a proxy to the device under test
    """
    assert not subarray.configureScanTimeoutExpiredFlag


def test_assignResourcesTimeoutExpiredFlag(
    subarray: tango.DeviceProxy,
) -> None:
    """
    Test for timeoutExpiredFlag.

    :param subarray: a proxy to the device under test
    """
    assert not subarray.assignResourcesTimeoutExpiredFlag


def test_releaseResourcesTimeoutExpiredFlag(
    subarray: tango.DeviceProxy,
) -> None:
    """
    Test for timeoutExpiredFlag.

    :param subarray: a proxy to the device under test
    """
    assert not subarray.releaseResourcesTimeoutExpiredFlag


# TODO: Pylint is right that this test is way too long.
@pytest.mark.parametrize("command_alias", ["ConfigureScan"])
def test_ConfigureScan_and_GoToIdle(  # pylint: disable=too-many-statements
    subarray: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
    command_alias: str,
) -> None:
    """
    Test for ConfigureScan.

    :param subarray: a proxy to the device under test
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    :param command_alias: name of the specific command being tested.
    """
    assert subarray.state() == tango.DevState.OFF

    for attribute in [
        "state",
        "status",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        subarray.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )

    change_event_callbacks["state"].assert_change_event(tango.DevState.OFF)
    change_event_callbacks["status"].assert_change_event("The device is in OFF state.")
    change_event_callbacks["longRunningCommandProgress"].assert_change_event(())
    change_event_callbacks["longRunningCommandStatus"].assert_change_event(())
    change_event_callbacks["longRunningCommandResult"].assert_change_event(("", ""))

    [[result_code], [on_command_id]] = subarray.On()
    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "QUEUED")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "IN_PROGRESS")
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress", (on_command_id, progress_point)
        )

    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    change_event_callbacks.assert_change_event("status", "The device is in ON state.")

    assert subarray.state() == tango.DevState.ON

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            on_command_id,
            json.dumps([int(ResultCode.OK), "On command completed OK"]),
        ),
    )

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "COMPLETED")
    )

    subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["obsState"],
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.EMPTY)

    [
        [result_code],
        [assign_command_id],
    ] = subarray.AssignResources('{"resource_ids":[1, 2]}')

    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event("obsState", ObsState.RESOURCING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "STAGING"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "QUEUED"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "IN_PROGRESS"),
    )
    assert subarray.assignedResources == (
        "reference/resource/01",
        "reference/resource/02",
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress",
            (assign_command_id, progress_point),
        )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            assign_command_id,
            json.dumps([int(ResultCode.OK), "Resource assignment completed OK"]),
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "COMPLETED"),
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)

    # TODO: Everything above here is just to turn on the device, assign it some
    # resources, and clear the queue attributes. We need a better way to handle
    # this.
    assert subarray.configurationId == ""

    configuration_id = "sbi-mvp01-20200325-00002"
    [[result_code], [config_command_id]] = subarray.command_inout(
        command_alias, '{"config_id": "sbi-mvp01-20200325-00002"}'
    )
    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event("obsState", ObsState.CONFIGURING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "STAGING",
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "QUEUED",
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "IN_PROGRESS",
        ),
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress",
            (config_command_id, progress_point),
        )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            config_command_id,
            json.dumps([int(ResultCode.OK), "Configure completed OK"]),
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "COMPLETED",
        ),
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.READY)
    assert subarray.configurationId == configuration_id
    assert subarray.lastScanConfiguration == json.dumps({"config_id": configuration_id})

    # test deconfigure
    [[result_code], [gotoidle_command_id]] = subarray.GoToIdle()
    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "COMPLETED",
            gotoidle_command_id,
            "STAGING",
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "COMPLETED",
            gotoidle_command_id,
            "QUEUED",
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "COMPLETED",
            gotoidle_command_id,
            "IN_PROGRESS",
        ),
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress",
            (gotoidle_command_id, progress_point),
        )
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            gotoidle_command_id,
            json.dumps([int(ResultCode.OK), "Deconfigure completed OK"]),
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (
            on_command_id,
            "COMPLETED",
            assign_command_id,
            "COMPLETED",
            config_command_id,
            "COMPLETED",
            gotoidle_command_id,
            "COMPLETED",
        ),
    )

    assert subarray.configurationID == ""
    assert subarray.lastScanConfiguration == ""


def test_ConfigureScan_when_in_wrong_state(
    subarray: tango.DeviceProxy,
) -> None:
    """
    Test for ConfigureScan when the device is in wrong state.

    :param subarray: a proxy to the device under test
    """
    # The device in in OFF/EMPTY state, not valid to invoke ConfigureScan.
    with pytest.raises(
        tango.DevFailed,
        match="ConfigureScan command not permitted in observation state EMPTY",
    ):
        subarray.ConfigureScan(
            '{"config_id":"sbi-mvp01-20200325-00002"}'
        )  # noqa: FS003


def test_ConfigureScan_with_wrong_configId_key(
    subarray: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test that ConfigureScan handles a wrong configuration id key.

    :param subarray: a proxy to the device under test
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    """
    assert subarray.state() == tango.DevState.OFF

    for attribute in [
        "state",
        "status",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        subarray.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )

    change_event_callbacks["state"].assert_change_event(tango.DevState.OFF)
    change_event_callbacks["status"].assert_change_event("The device is in OFF state.")
    change_event_callbacks["longRunningCommandProgress"].assert_change_event(())
    change_event_callbacks["longRunningCommandStatus"].assert_change_event(())
    change_event_callbacks["longRunningCommandResult"].assert_change_event(("", ""))

    [[result_code], [on_command_id]] = subarray.On()
    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "QUEUED")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "IN_PROGRESS")
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress", (on_command_id, progress_point)
        )

    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    change_event_callbacks.assert_change_event("status", "The device is in ON state.")

    assert subarray.state() == tango.DevState.ON

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            on_command_id,
            json.dumps([int(ResultCode.OK), "On command completed OK"]),
        ),
    )

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "COMPLETED")
    )

    subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["obsState"],
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.EMPTY)

    [
        [result_code],
        [assign_command_id],
    ] = subarray.AssignResources('{"resource_ids":[1, 2, 3]}')

    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event("obsState", ObsState.RESOURCING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "STAGING"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "QUEUED"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "IN_PROGRESS"),
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress",
            (assign_command_id, progress_point),
        )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            assign_command_id,
            json.dumps([int(ResultCode.OK), "Resource assignment completed OK"]),
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "COMPLETED"),
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)

    wrong_configuration = {"config_id": 2223}  # noqa: FS003
    with pytest.raises(tango.DevFailed, match="2223 is not of type 'string'"):
        subarray.ConfigureScan(json.dumps(wrong_configuration))
        assert subarray.obsState == ObsState.IDLE


def test_ConfigureScan_with_json_syntax_error(
    subarray: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test for ConfigureScan when syntax error in json configuration.

    :param subarray: a proxy to the device under test
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    """
    assert subarray.state() == tango.DevState.OFF

    for attribute in [
        "state",
        "status",
        "longRunningCommandProgress",
        "longRunningCommandStatus",
        "longRunningCommandResult",
    ]:
        subarray.subscribe_event(
            attribute,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute],
        )

    change_event_callbacks["state"].assert_change_event(tango.DevState.OFF)
    change_event_callbacks["status"].assert_change_event("The device is in OFF state.")
    change_event_callbacks["longRunningCommandProgress"].assert_change_event(())
    change_event_callbacks["longRunningCommandStatus"].assert_change_event(())
    change_event_callbacks["longRunningCommandResult"].assert_change_event(("", ""))

    [[result_code], [on_command_id]] = subarray.On()
    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "STAGING")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "QUEUED")
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "IN_PROGRESS")
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress", (on_command_id, progress_point)
        )

    change_event_callbacks.assert_change_event("state", tango.DevState.ON)
    change_event_callbacks.assert_change_event("status", "The device is in ON state.")

    assert subarray.state() == tango.DevState.ON

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            on_command_id,
            json.dumps([int(ResultCode.OK), "On command completed OK"]),
        ),
    )

    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus", (on_command_id, "COMPLETED")
    )

    subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["obsState"],
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.EMPTY)

    [
        [result_code],
        [assign_command_id],
    ] = subarray.AssignResources('{"resource_ids":[1, 2, 3]}')

    assert result_code == ResultCode.QUEUED

    change_event_callbacks.assert_change_event("obsState", ObsState.RESOURCING)
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "STAGING"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "QUEUED"),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "IN_PROGRESS"),
    )
    for progress_point in FakeCspSubarrayComponent.PROGRESS_REPORTING_POINTS:
        change_event_callbacks.assert_change_event(
            "longRunningCommandProgress",
            (assign_command_id, progress_point),
        )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (
            assign_command_id,
            json.dumps([int(ResultCode.OK), "Resource assignment completed OK"]),
        ),
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandStatus",
        (on_command_id, "COMPLETED", assign_command_id, "COMPLETED"),
    )
    change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)

    with pytest.raises(
        tango.DevFailed, match="jsonschema.exceptions.ValidationError: 'config_id'"
    ):
        subarray.ConfigureScan('{"foo": 1}')  # noqa: FS003
        assert subarray.obsState == ObsState.IDLE
