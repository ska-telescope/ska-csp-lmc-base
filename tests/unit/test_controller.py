# pylint: disable=invalid-name
"""Contain the tests for the CspSubelementController."""
from __future__ import annotations

import gc
import re
from typing import Generator

import pytest
import pytest_mock
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    ResultCode,
    SimulationMode,
    TestMode,
)
from ska_tango_base import SKAController
from ska_tango_base.testing.reference import ReferenceBaseComponentManager
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango.test_context import MultiDeviceTestContext

from ska_csp_lmc_base import CspSubElementController

# TODO: Weird hang-at-garbage-collection bug
gc.disable()


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup("state")


@pytest.fixture(name="controller_name", scope="session")
def controller_name_fixture() -> str:
    """
    Return the name of the controller Tango device.

    :return: the name of the controller Tango device.
    """
    return "low-test/control/1"


@pytest.fixture(name="patched_controller_device_class")
def patched_controller_device_class_fixture() -> type[CspSubElementController]:
    """
    Return a controller device class patched with a non-abstract component manager.

    :return: a controller device class patched with a non-abstract
        component manager.
    """

    class PatchedControllerDevice(CspSubElementController):
        """CspSubElementController patched with a non-abstract component manager."""

        def create_component_manager(
            self: PatchedControllerDevice,
        ) -> ReferenceBaseComponentManager:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            return ReferenceBaseComponentManager(
                self.logger,
                self._communication_state_changed,
                self._component_state_changed,
            )

    return PatchedControllerDevice


@pytest.fixture(name="power_delay_standby_on")
def power_delay_standby_on_fixture() -> float:
    """
    Return the power delay standby on value.

    :return: the power delay standby on value.
    """
    return 1.5


@pytest.fixture(name="power_delay_standby_off")
def power_delay_standby_off_fixture() -> float:
    """
    Return the power delay standby off value.

    :return: the power delay standby off value.
    """
    return 1.0


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    controller_name: str,
    patched_controller_device_class: type[CspSubElementController],
    power_delay_standby_off: float,
    power_delay_standby_on: float,
) -> Generator[TangoContextProtocol, None, None]:
    """
    Return a Tango harness against which to run tests of the deployment.

    :param controller_name: the name of the CSP controller Tango device
    :param patched_controller_device_class: a subclass of
        CspSubElementController been patched with extra commands that
        mock system under control behaviours.
    :param power_delay_standby_off: fixture that returns the power delay
        for standby -> off.
    :param power_delay_standby_on: fixture that returns the power delay
        for standby -> on.

    :yields: a tango context.
    """
    context_manager = ThreadedTestTangoContextManager()
    context_manager.add_device(
        controller_name,
        patched_controller_device_class,
        PowerDelayStandbyOn=power_delay_standby_on,
        PowerDelayStandbyOff=power_delay_standby_off,
    )
    with context_manager as context:
        yield context


@pytest.fixture(name="controller")
def controller_fixture(
    tango_harness: TangoContextProtocol,
    controller_name: str,
) -> tango.DeviceProxy:
    """
    Fixture that returns the controller Tango device under test.

    :param tango_harness: a test harness for Tango devices.
    :param controller_name: name of the controller Tango device.

    :returns: the controller Tango device under test.
    """
    controller_device = tango_harness.get_device(controller_name)
    controller_device.adminMode = AdminMode.ONLINE
    return controller_device


class TestCspSubElementController:  # pylint: disable=too-many-public-methods
    """Test case for CSP SubElement Controller class."""

    @pytest.mark.skip("Not implemented")
    def test_properties(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test device properties.

        :param controller: a proxy to the device under test
        """

    def test_State(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for State.

        :param controller: a proxy to the device under test
        """
        assert controller.state() == tango.DevState.OFF

    def test_Status(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for Status.

        :param controller: a proxy to the device under test
        """
        assert controller.Status() == "The device is in OFF state."

    def test_GetVersionInfo(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for GetVersionInfo.

        :param controller: a proxy to the device under test
        """
        version_pattern = (
            f"{controller.info().dev_class}, ska_tango_base, "
            "[0-9]+.[0-9]+.[0-9]+, A set of generic base devices for SKA Telescope."
        )
        version_info = controller.GetVersionInfo()
        assert len(version_info) == 1
        assert re.match(version_pattern, version_info[0])

    def test_buildState(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for buildState.

        :param controller: a proxy to the device under test
        """
        build_pattern = re.compile(
            r"ska_tango_base, [0-9]+.[0-9]+.[0-9]+, "
            r"A set of generic base devices for SKA Telescope"
        )
        assert (re.match(build_pattern, controller.buildState)) is not None

    def test_versionId(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for versionId.

        :param controller: a proxy to the device under test
        """
        version_id_pattern = re.compile(r"[0-9]+.[0-9]+.[0-9]+")
        assert (re.match(version_id_pattern, controller.versionId)) is not None

    def test_healthState(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for healthState.

        :param controller: a proxy to the device under test
        """
        assert controller.healthState == HealthState.UNKNOWN

    def test_adminMode(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for adminMode.

        :param controller: a proxy to the device under test
        """
        assert controller.adminMode == AdminMode.ONLINE

    def test_controlMode(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for controlMode.

        :param controller: a proxy to the device under test
        """
        assert controller.controlMode == ControlMode.REMOTE

    def test_simulationMode(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for simulationMode.

        :param controller: a proxy to the device under test
        """
        assert controller.simulationMode == SimulationMode.FALSE

    def test_testMode(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for testMode.

        :param controller: a proxy to the device under test
        """
        assert controller.testMode == TestMode.NONE

    def test_powerDelayStandbyOn(
        self: TestCspSubElementController,
        controller: tango.DeviceProxy,
        power_delay_standby_on: float,
    ) -> None:
        """
        Test for powerDelayStandbyOn.

        :param controller: a proxy to the device under test
        :param power_delay_standby_on: fixture that returns the power
            delay for standby -> on
        """
        assert controller.powerDelayStandbyOn == pytest.approx(power_delay_standby_on)
        controller.powerDelayStandbyOn = 3
        assert controller.powerDelayStandbyOn == 3

    def test_powerDelayStandbyOff(
        self: TestCspSubElementController,
        controller: tango.DeviceProxy,
        power_delay_standby_off: float,
    ) -> None:
        """
        Test for powerDelayStandbyOff.

        :param controller: a proxy to the device under test
        :param power_delay_standby_off: fixture that returns the power
            delay for standby -> off
        """
        assert controller.powerDelayStandbyOff == pytest.approx(power_delay_standby_off)
        controller.powerDelayStandbyOff = 2
        assert controller.powerDelayStandbyOff == 2

    def test_onProgress(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for onProgress.

        :param controller: a proxy to the device under test
        """
        assert controller.onProgress == 0

    def test_onMaximumDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for onMaximumDuration.

        :param controller: a proxy to the device under test
        """
        controller.onMaximumDuration = 5
        assert controller.onMaximumDuration == 5

    def test_onMeasuredDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for onMeasuredDuration.

        :param controller: a proxy to the device under test
        """
        assert controller.onMeasuredDuration == 0

    def test_standbyProgress(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for standbyProgress.

        :param controller: a proxy to the device under test
        """
        assert controller.standbyProgress == 0

    def test_standbyMaximumDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for standbyMaximumDuration.

        :param controller: a proxy to the device under test
        """
        controller.standbyMaximumDuration = 5
        assert controller.standbyMaximumDuration == 5

    def test_standbyMeasuredDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for standbyMeasuredDuration.

        :param controller: a proxy to the device under test
        """
        assert controller.standbyMeasuredDuration == 0

    def test_offProgress(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for offProgress.

        :param controller: a proxy to the device under test
        """
        assert controller.offProgress == 0

    def test_offMaximumDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for offMaximumDuration.

        :param controller: a proxy to the device under test
        """
        controller.offMaximumDuration = 5
        assert controller.offMaximumDuration == 5

    def test_offMeasuredDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for offMeasuredDuration.

        :param controller: a proxy to the device under test
        """
        assert controller.offMeasuredDuration == 0

    def test_loadFirmwareProgress(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for loadFirmwareProgress.

        :param controller: a proxy to the device under test
        """
        assert controller.loadFirmwareProgress == 0

    def test_loadFirmwareMaximumDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for loadFirmwareMaximumDuration.

        :param controller: a proxy to the device under test
        """
        controller.loadFirmwareMaximumDuration = 5
        assert controller.loadFirmwareMaximumDuration == 5

    def test_loadFirmwareMeasuredDuration(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for loadFirmwareMeasuredDuration.

        :param controller: a proxy to the device under test
        """
        assert controller.loadFirmwareMeasuredDuration == 0

    def test_LoadFirmware(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for LoadFirmware.

        :param controller: a proxy to the device under test
        """
        # After initialization the device is in the right state (OFF/ENGINEERING) to
        # execute the command.
        controller.adminMode = AdminMode.ENGINEERING
        assert controller.LoadFirmware(["file", "test/dev/b", "918698a7fea3"]) == [
            [ResultCode.OK],
            ["LoadFirmware command completed OK"],
        ]

    def test_LoadFirmware_when_in_wrong_state(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for LoadFirmware when the device is in wrong state.

        :param controller: a proxy to the device under test
        """
        # Set the device in ON/ONLINE state
        controller.On()
        with pytest.raises(
            tango.DevFailed,
            match="LoadFirmware not allowed when the device is in OFF state",
        ):
            controller.LoadFirmware(["file", "test/dev/b", "918698a7fea3"])

    def test_power_on_and_off_devices(
        self: TestCspSubElementController,
        controller: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for PowerOnDevices.

        :param controller: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support
        """
        assert controller.state() == tango.DevState.OFF

        controller.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.OFF)

        [[result_code], [_]] = controller.On()
        assert result_code == ResultCode.QUEUED

        change_event_callbacks.assert_change_event("state", tango.DevState.ON)
        assert controller.state() == tango.DevState.ON

        # Test power on devices
        [[result_code], [_]] = controller.PowerOnDevices(["test/dev/1", "test/dev/2"])
        assert result_code == ResultCode.OK

        # Test power off devices
        [[result_code], [_]] = controller.PowerOffDevices(["test/dev/1", "test/dev/2"])
        assert result_code == ResultCode.OK

    def test_PowerOnDevices_when_in_wrong_state(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for PowerOnDevices when the Controller is in wrong state.

        :param controller: a proxy to the device under test
        """
        with pytest.raises(
            tango.DevFailed,
            match="Command PowerOnDevices not allowed when the device is in OFF state",
        ):
            controller.PowerOnDevices(["test/dev/1", "test/dev/2"])

    def test_PowerOffDevices_when_in_wrong_state(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for PowerOffDevices when the Controller is in wrong state.

        :param controller: a proxy to the device under test
        """
        with pytest.raises(
            tango.DevFailed,
            match="Command PowerOffDevices not allowed when the device is in OFF state",
        ):
            controller.PowerOffDevices(["test/dev/1", "test/dev/2"])

    def test_ReInitDevices(
        self: TestCspSubElementController,
        controller: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for ReInitDevices.

        :param controller: a proxy to the device under test
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support
        """
        assert controller.state() == tango.DevState.OFF

        controller.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.OFF)

        [[result_code], [_]] = controller.On()
        assert result_code == ResultCode.QUEUED

        change_event_callbacks.assert_change_event("state", tango.DevState.ON)
        assert controller.state() == tango.DevState.ON

        # Test power on devices
        [[result_code], [_]] = controller.PowerOnDevices(["test/dev/1", "test/dev/2"])
        assert controller.ReInitDevices(["test/dev/1", "test/dev/2"]) == [
            [ResultCode.OK],
            ["ReInitDevices command completed OK"],
        ]

    def test_ReInitDevices_when_in_wrong_state(
        self: TestCspSubElementController, controller: tango.DeviceProxy
    ) -> None:
        """
        Test for ReInitDevices whe the device is in a wrong state.

        :param controller: a proxy to the device under test
        """
        # put it in ON state
        with pytest.raises(
            tango.DevFailed,
            match="ReInitDevices not allowed when the device is in OFF state",
        ):
            controller.ReInitDevices(["test/dev/1", "test/dev/2"])


@pytest.mark.forked
def test_multiple_devices_in_same_process(
    patched_controller_device_class: type[CspSubElementController],
    mocker: pytest_mock.MockerFixture,
) -> None:
    """
    Test that we can run this device with other devices in a single process.

    :param mocker: pytest fixture that wraps :py:mod:`unittest.mock`.
    :param patched_controller_device_class: a subclass of
        CspSubElementController been patched with extra commands that
        mock system under control behaviours.

    """
    # Patch abstract method/s; it doesn't matter what we patch them with, so long as
    # they don't raise NotImplementedError.
    component_manager = ReferenceBaseComponentManager(
        mocker.MagicMock(), mocker.MagicMock(), mocker.MagicMock()
    )
    mocker.patch(
        "ska_tango_base.controller_device.SKAController.create_component_manager",
        return_value=component_manager,
    )

    devices_info = (
        {"class": patched_controller_device_class, "devices": [{"name": "test/se/1"}]},
        {"class": SKAController, "devices": [{"name": "test/control/1"}]},
    )

    with MultiDeviceTestContext(devices_info, process=False) as context:
        proxy1 = context.get_device("test/se/1")
        proxy2 = context.get_device("test/control/1")
        assert proxy1.state() == tango.DevState.DISABLE
        assert proxy2.state() == tango.DevState.DISABLE
